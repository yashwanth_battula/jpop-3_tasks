package com.epam.restControllerTest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.epam.customexceptions.BookException;
import com.epam.dto.BookDTO;
import com.epam.entity.Book;
import com.epam.restcontroller.BookRest;
import com.epam.service.BookService;

public class BookRestControllerTest {

	@Mock
	BookService bookService;
	
	@InjectMocks
	BookRest bookRest;
	
	private Object mockMVC;

	@BeforeEach
	public void init() {
		MockitoAnnotations.initMocks(this);
		this.mockMVC = MockMvcBuilders.standaloneSetup(bookRest).build();
	}
	
	@Test
	void testGetAllBooks() {
		List<BookDTO> booksList = new ArrayList<>();
		booksList.add(new BookDTO(1L,"ab","ab","ab","ab"));
		when(bookService.findAllBooks()).thenReturn(booksList);
		ResponseEntity<List<BookDTO>> responseEntity = bookRest.findAllBooks();
		assertThat(responseEntity.getBody().equals(booksList));
	}
	
	@Test
	void testGetBookById() throws BookException {
		BookDTO bookDto = new BookDTO(1L,"abc","abc","abc","abc");
		when(bookService.findBookById(1L)).thenReturn(bookDto);
		ResponseEntity<BookDTO> responseEntity = bookRest.findBookById(1L);
		assertThat(responseEntity.getBody().equals(bookDto));
	}
	
	@Test
	void testAddNewBook(){
		BookDTO bookDto = new BookDTO(1L,"abc","abc","abc","abc");
		when(bookService.addNewBook(bookDto)).thenReturn(bookDto);
		ResponseEntity<BookDTO> responseEntity = bookRest.addNewBook(bookDto);
		assertThat(responseEntity.getBody().equals(bookDto));
	}
	
	@Test
	void testDeleteBook() throws BookException {
		BookDTO bookDto = new BookDTO(1L,"abc","abc","abc","abc");
		when(bookService.deleteBookById(1L)).thenReturn(true);
		ResponseEntity responseEntity = bookRest.deleteBookById(1L);
		assertThat(responseEntity.getStatusCode().equals(HttpStatus.OK));
	}
	
	@Test
	void testUpdateBook() throws BookException{
		BookDTO bookDto = new BookDTO(1L,"abc","abc","abc","abc");
		when(bookService.updateBook(1L, bookDto)).thenReturn(bookDto);
		ResponseEntity<BookDTO> responseEntity = bookRest.updateBook(1L, bookDto);
		assertThat(responseEntity.getBody().equals(bookDto));
	}

}
