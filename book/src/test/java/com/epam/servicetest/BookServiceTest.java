package com.epam.servicetest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.epam.customexceptions.BookException;
import com.epam.dto.BookDTO;
import com.epam.entity.Book;
import com.epam.repository.BookRepository;
import com.epam.service.BookService;

public class BookServiceTest {

	@Mock
	BookRepository bookRepository;
	
	@InjectMocks
	BookService bookService;
	
	@BeforeEach
	public void init() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testFindAllBooks() {
		List<Book> booksList = new ArrayList<>();
		Book book = new Book(1L,"abc","abc","abc","abc");
		booksList.add(book);
		when(bookRepository.findAll()).thenReturn(booksList);
		List<BookDTO> bookDTOList = new ArrayList<>();
		bookDTOList.add(new BookDTO(book));
		assertEquals(bookDTOList.size(), bookService.findAllBooks().size());
	}
	
	@Test
	public void testFindBookById() throws BookException {
		Book book = new Book(1L,"abc","abc","abc","abc");
		Optional<Book> bookOptional = Optional.ofNullable(book);
		when(bookRepository.findById(1L)).thenReturn(bookOptional);
		BookDTO expectedBook = new BookDTO(book);
		BookDTO actualBook = bookService.findBookById(1L);
		assertEquals(expectedBook.getId(), actualBook.getId());
		assertEquals(expectedBook.getAuthor(), actualBook.getAuthor());
		assertEquals(expectedBook.getGenre(), actualBook.getGenre());
		assertEquals(expectedBook.getTitle(), actualBook.getAuthor());
		assertEquals(expectedBook.getPrice(), actualBook.getPrice());
	}
	
	@Test
	public void testAddNewBook() {
		BookDTO expectedBook = new BookDTO(1L,"abc","abc","abc","abc");
		BookDTO actualBook = bookService.addNewBook(expectedBook);
		assertEquals(expectedBook.getAuthor(), actualBook.getAuthor());
		assertEquals(expectedBook.getGenre(), actualBook.getGenre());
		assertEquals(expectedBook.getTitle(), actualBook.getAuthor());
		assertEquals(expectedBook.getPrice(), actualBook.getPrice());
	}
	
	@Test
	public void testDeleteBookById() throws BookException {
		Book book = new Book(1L,"abc","abc","abc","abc");
		Optional<Book> bookOptional = Optional.ofNullable(book);
		when(bookRepository.findById(1L)).thenReturn(bookOptional);
		assertEquals(true , bookService.deleteBookById(1L));
	}
	
	@Test
	public void testUpdateBook() throws BookException {
		Book book = new Book(1L,"abc","abc","abc","abc");
		Optional<Book> bookOptional = Optional.ofNullable(book);
		when(bookRepository.findById(1L)).thenReturn(bookOptional);
		BookDTO expectedBook = new BookDTO(1L,"abcd","abcd","abcd","abcd");
		BookDTO actualBook = bookService.updateBook(1L, expectedBook);
		assertEquals(expectedBook.getAuthor(), actualBook.getAuthor());
		assertEquals(expectedBook.getGenre(), actualBook.getGenre());
		assertEquals(expectedBook.getTitle(), actualBook.getAuthor());
		assertEquals(expectedBook.getPrice(), actualBook.getPrice());
	}
}
