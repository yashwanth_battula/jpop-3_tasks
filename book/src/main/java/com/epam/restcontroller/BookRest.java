package com.epam.restcontroller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.epam.customexceptions.BookException;
import com.epam.dto.BookDTO;
import com.epam.service.BookService;

@RestController
public class BookRest {

	@Autowired
	BookService bookService;
	
	@GetMapping("/book")
	public ResponseEntity<List<BookDTO>> findAllBooks(){
		return ResponseEntity.ok(bookService.findAllBooks());
	}
	
	@GetMapping("/book/{bookId}")
	public ResponseEntity<BookDTO> findBookById(@PathVariable(value = "bookId") Long id){
		ResponseEntity<BookDTO> response;
		try {
			response = new ResponseEntity<>(bookService.findBookById(id) , HttpStatus.OK);
		}catch(BookException e){
			response = new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		return response;
	}
	
	@PostMapping("/book")
	public ResponseEntity<BookDTO> addNewBook(@RequestBody BookDTO bookDTO){
		return ResponseEntity.ok(bookService.addNewBook(bookDTO));
	}
	
	@DeleteMapping("/book/{bookId}")
	public ResponseEntity<?> deleteBookById(@PathVariable(value = "bookId") Long id){
		ResponseEntity<?> response;
		try {
			bookService.deleteBookById(id);
			response = new ResponseEntity<>(HttpStatus.OK);
		}catch(BookException e){
			response = new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		return response;
	}
	
	@PutMapping("/book/{bookId}")
	public ResponseEntity<BookDTO> updateBook(@PathVariable(value = "bookId") Long id,@RequestBody BookDTO bookDTO){
		ResponseEntity<BookDTO> response;
		try {
			response = new ResponseEntity<>(bookService.updateBook(id,bookDTO) , HttpStatus.OK);
		}catch(BookException e){
			response = new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		return response;
	}
	
}
