package com.epam.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.customexceptions.BookException;
import com.epam.dto.BookDTO;
import com.epam.entity.Book;
import com.epam.repository.BookRepository;

@Service
public class BookService {

	@Autowired
	BookRepository bookRepository;

	public List<BookDTO> findAllBooks() {
		List<Book> bookList = bookRepository.findAll();
		List<BookDTO> bookDTOList = new ArrayList<>();
		for(Book book:bookList) {
			bookDTOList.add(new BookDTO(book));
		}
		return bookDTOList;
	}

	public BookDTO findBookById(Long id) throws BookException {
		Book book = bookRepository.findById(id).orElseThrow(() -> new BookException("Book not found"));
		return new BookDTO(book);
	}

	public BookDTO addNewBook(BookDTO bookDTO) {
		Book book = new Book(bookDTO.getId() , bookDTO.getTitle() , bookDTO.getAuthor() , bookDTO.getGenre() , bookDTO.getPrice());
		Book bookAdded = bookRepository.save(book);
		return new BookDTO(bookAdded);
	}

	public Boolean deleteBookById(Long id) throws BookException {
		Boolean isDeleted = false;
		if(bookRepository.findById(id).isPresent()) {
			bookRepository.deleteById(id);
			isDeleted = true;
		}else {
			throw new BookException("Book not found with id:"+id);
		}
		return isDeleted;
	}

	public BookDTO updateBook(Long id, BookDTO bookDTO) throws BookException {
		Book newBook = new Book(id , bookDTO.getTitle() , bookDTO.getAuthor() , bookDTO.getGenre() , bookDTO.getPrice());
		bookRepository.findById(id).map(oldBook -> bookRepository.save(newBook)).orElseThrow(() -> new BookException("book not found with id"+id));
		return new BookDTO(newBook);
	}
	
}
