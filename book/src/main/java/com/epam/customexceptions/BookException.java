package com.epam.customexceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class BookException extends Exception{

	public BookException(String exceptionMsg) {
		super(exceptionMsg);
	}
}
